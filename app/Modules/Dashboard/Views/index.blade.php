@extends('backend.layouts.app')
@section('content')
    <!-- Info boxes -->
    <div class="row">
        <!-- fix for small devices only -->
        <div class="clearfix hidden-md-up"></div>
        <div class="col-12 col-sm-3 col-md-3">
            <p>Welcome to Dashboard <strong class="text-success">{{ auth()->user()->name }} </strong></p>
        </div>

    </div>
    <!-- /.row -->
@endsection
