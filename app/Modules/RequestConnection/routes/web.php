<?php

use Illuminate\Support\Facades\Route;

Route::group(['module' => 'RequestConnection', 'middleware' => ['web'], 'namespace' => 'App\Modules\RequestConnection\Controllers'], function() {

    Route::get('request-connections/{id}/delete','RequestConnectionController@delete');
    Route::resource('request-connections', 'RequestConnectionController');

});
