<?php

Route::group(['module' => 'RequestConnection', 'middleware' => ['api'], 'namespace' => 'App\Modules\RequestConnection\Controllers'], function() {

    Route::resource('RequestConnection', 'RequestConnectionController');

});
