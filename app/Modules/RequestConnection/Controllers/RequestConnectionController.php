<?php

namespace App\Modules\RequestConnection\Controllers;

use App\Libraries\CommonFunction;
use App\Libraries\Encryption;
use App\Modules\RequestConnection\Models\RequestConnection;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Carbon;
use Illuminate\Validation\Rule;

class RequestConnectionController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['requestConnections'] = RequestConnection::where(['is_archive'=>0,'status'=>1])->get();
        return view("RequestConnection::index",$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view("RequestConnection::create");
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'name'    => 'required',
            'email'   => 'required|email|unique:request_connections',
            'mobile'  => 'required',
            'request_date' => 'required',
            'address' => 'required',
            'status'  => 'required'
        ],[
            'mobile.required'  => 'The contact number field is required',
            'request_date.required' => 'The date field is required',
        ]);

        $requestConnection = new RequestConnection();
        $requestConnection->name = $request->input('name');
        $requestConnection->email = $request->input('email');
        $requestConnection->mobile = $request->input('mobile');
        $requestConnection->request_date =( $request->input('request_date'))?CommonFunction::dbDateFormat( $request->input('request_date')):null;
        $requestConnection->address = $request->input('address');
        $requestConnection->status = $request->input('status');
        $requestConnection->save();

        return redirect('request-connections')->with('flash_success','Request connection created successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $requestConnectionId
     * @return \Illuminate\Http\Response
     */
    public function edit($requestConnectionId)
    {
        $decodedRequestConnectionId = Encryption::decodeId($requestConnectionId);
        $data['requestConnection'] = RequestConnection::find($decodedRequestConnectionId);

        return view("RequestConnection::edit",$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $requestConnectionId
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $requestConnectionId)
    {
        $decodedRequestConnectionId = Encryption::decodeId($requestConnectionId);
        $this->validate($request,[
            'name'    => 'required',
            'email'   => ['required', 'email', Rule::unique('request_connections')->ignore($decodedRequestConnectionId)],
            'mobile'  => 'required',
            'request_date'  => 'required',
            'address' => 'required',
            'status'  => 'required'
        ],[
            'mobile.required'  => 'The contact number field is required',
            'request_date.required' => 'The date field is required',
        ]);

        $requestConnection = RequestConnection::find($decodedRequestConnectionId);
        $requestConnection->name = $request->input('name');
        $requestConnection->email = $request->input('email');
        $requestConnection->mobile = $request->input('mobile');
        $requestConnection->request_date =( $request->input('request_date'))?CommonFunction::dbDateFormat( $request->input('request_date')):null;
        $requestConnection->address = $request->input('address');
        $requestConnection->status = $request->input('status');
        $requestConnection->save();

        return redirect('request-connections')->with('flash_success','Request connection updated successfully.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $requestConnectionId
     * @return \Illuminate\Http\Response
     */
    public function delete($requestConnectionId)
    {
        $decodedRequestConnectionId = Encryption::decodeId($requestConnectionId);
        RequestConnection::where('id',$decodedRequestConnectionId)->delete();
        session()->flash('flash_success','Request connection deleted successfully.');
    }
}
