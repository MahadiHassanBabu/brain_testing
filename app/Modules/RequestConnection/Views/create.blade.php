@extends('backend.layouts.app')
@section('header-css')
    {!! Html::style('assets/backend/plugins/datetimepicker/css/bootstrap-timepicker.min.css') !!}
    {!! Html::style('assets/backend/plugins/datetimepicker/css/bootstrap-datetimepicker-standalone.css') !!}
    {!! Html::style('assets/backend/plugins/datetimepicker/css/bootstrap-datetimepicker2.min.css') !!}
@endsection
@section('content')
   <div class="card">
       <div class="card-header">
           <div class="row">
               <div class="col-sm-5">
                   <span class="card-title"><i class="fa fa-plus-square"></i> Add request connection</span>
               </div><!--col-->
           </div>
       </div>
       {!! Form::open(['route'=>'request-connections.store', 'method'=>'post','id'=>'dataForm']) !!}
       <div class="card-body">
           <div class="row">
               <div class="col-md-6">
                   <div class="form-group">
                       {!! Form::label('name','Name : ',['class'=>'required-star']) !!}
                       {!! Form::text('name','',['class'=>$errors->has('name')?'form-control is-invalid':'form-control required','placeholder'=>'Name']) !!}
                   </div>
               </div>
               <div class="col-md-6">
                   <div class="form-group">
                       {!! Form::label('email','Email Address : ',['class'=>'required-star']) !!}
                       {!! Form::email('email','',['class'=>$errors->has('email')?'form-control is-invalid':'form-control required','placeholder'=>'Email address']) !!}
                   </div>
               </div>
               <div class="col-md-6">
                   <div class="form-group">
                       {{ Form::label('mobile','Contact Number',['class'=>'required-star']) }}
                       <div class="input-group">
                           <div class="input-group-prepend">
                               <span class="input-group-text">+88</span>
                           </div>
                           {!! Form::text('mobile','',['class'=>$errors->has('mobile')?'form-control is-invalid':'form-control required','minlength'=>'11','maxlength'=>'11','placeholder'=>'Mobile']) !!}
                       </div>
                   </div>
               </div>
               <div class="col-md-6">
                   <div class="form-group">
                       {!! Form::label('address','Address : ',['class'=>'required-star']) !!}
                       {!! Form::textarea('address','',['class'=>$errors->has('address')?'form-control is-invalid':'form-control required','maxlength'=>'80','rows'=>1]) !!}
                   </div>
               </div>
               <div class="col-md-6">
                   <div class="form-group">
                       {!! Form::label('request_date','Date : ',['class'=>'required-star']) !!}
                       {!! Form::text('request_date','',['class'=>'required form-control date-picker '.($errors->has('request_date')?'is-invalid':''),'autocomplete'=>'off','placeholder'=>'DD/MM/YYYY']) !!}
                   </div>
               </div>
               <div class="col-md-6">
                   <div class="form-group">
                       {!! Form::label('status','Status : ',['class'=>'required-star']) !!}
                       {!! Form::select('status',['1'=>'Active','0'=>'Inactive'],'',['class'=>$errors->has('status')?'form-control is-invalid':'form-control required']) !!}
                   </div>
               </div>
           </div>
       </div>
       <div class="card-footer">
           <a href="{{ route('request-connections.index') }}" class="btn btn-warning"><i class="fa fa-backward"></i> Back</a>
           <button type="submit" class="btn float-right btn-primary"><i class="fa fa-save"></i> Save</button>
       </div>
       {!! Form::close() !!}
   </div>
@endsection
@section('footer-script')

    {!! Html::script('assets/backend/plugins/datetimepicker/js/bootstrap-timepicker.min.js') !!}
    {!! Html::script('assets/backend/plugins/datetimepicker/js/bootstrap.min.js') !!}
    {!! Html::script('assets/backend/plugins/datetimepicker/js/moment.min.js') !!}
    {!! Html::script('assets/backend/plugins/datetimepicker/js/bootstrap-datetimepicker2.min.js') !!}
    <script type="text/javascript">
        $('.date-picker').datetimepicker({
            format: 'DD/MM/YYYY',
            icons: {
                date: "fa fa-calendar",
                previous:"fa fa-angle-left",
                next:"fa fa-angle-right"
            }
        });
    </script>
@endsection
