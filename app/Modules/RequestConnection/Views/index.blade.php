@extends('backend.layouts.app')
@section('header-css')
    {!! Html::style('assets/backend/dist/css/dataTables.bootstrap4.min.css') !!}
    {!! Html::style('assets/backend/dist/css/buttons.dataTables.min.css') !!}
@endsection
@section('content')
    <div class="card">
        <div class="card-header">
            <div class="row">
                <div class="col-sm-5">
                    <h5><i class="fa fa-list-alt"></i> Requests Connection List</h5>
                </div><!--col-->

                <div class="col-sm-7 pull-right">
                    <div class="btn-toolbar float-right" role="toolbar" aria-label="Toolbar with button groups">
                        <a href="{{ route('request-connections.create') }}" class="btn btn-sm btn-success" title="Create new" data-original-title="Create New">
                            <i class="fa fa-plus-circle"></i> Create
                        </a>
                    </div>
                </div><!--col-->
            </div>
        </div>
        <div class="card-body">
            <div class="row mt-4">
                <div class="col-md-12">
                    <table class="table table-striped table-borderless">
                        <thead>
                        <tr>
                            <th>Name</th>
                            <th>Email</th>
                            <th>Contact Number</th>
                            <th>Date</th>
                            <th>Status</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @if(count($requestConnections) > 0)
                            @foreach($requestConnections as $requestConnection)
                                <tr>
                                    <td>{{ ($requestConnection->name) ? $requestConnection->name : 'N/A' }}</td>
                                    <td>{{ ($requestConnection->email) ? $requestConnection->email : 'N/A' }}</td>
                                    <td>{{ ($requestConnection->mobile) ? $requestConnection->mobile : 'N/A' }}</td>
                                    <td>{{ ($requestConnection->request_date) ? \Carbon\Carbon::parse($requestConnection->request_date)->format('d F, Y') : 'N/A' }}</td>
                                    <td>
                                        @if ($requestConnection->status)
                                            <span class="badge badge-success">Active</span>
                                        @else
                                            <span class='badge badge-danger'>Inactive</span>
                                        @endif
                                    </td>
                                    <td>
                                        <a href="{{ url('/request-connections/'.\App\Libraries\Encryption::encodeId($requestConnection->id)).'/edit/' }}" class="btn btn-sm btn-primary" title="Edit">
                                            <i class="fa fa-edit"></i> Edit
                                        </a>
                                        <a href="{{ url('/request-connections/'.\App\Libraries\Encryption::encodeId($requestConnection->id)).'/delete/' }}"
                                           redirect-url="{{ url('/request-connections') }}" class="btn btn-sm btn-danger action-delete" title="Delete">
                                            <i class="fa fa-trash"></i> Delete
                                        </a>
                                    </td>
                                </tr>
                            @endforeach
                        @endif
                        </tbody>
                    </table>
                </div>
            </div><!--row-->
        </div><!--card-body-->
    </div><!--card-->

@endsection

@section('footer-script')
    {!! Html::script('assets/backend/dist/js/jquery.dataTables.min.js') !!}
    {!! Html::script('assets/backend/dist/js/dataTables.bootstrap4.min.js') !!}
    {!! Html::script('assets/backend/dist/js/dataTables.buttons.min.js') !!}

    <script type="text/javascript">
        $('table').DataTable();

        $(document.body).on('click','.action-delete',function(ev){
            ev.preventDefault();
            let URL = $(this).attr('href');
            let redirectURL = "{{ route('request-connections.index') }}";
            warnBeforeAction(URL, redirectURL);
        });
    </script>
@endsection
