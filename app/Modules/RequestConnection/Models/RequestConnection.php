<?php

namespace App\Modules\RequestConnection\Models;

use Illuminate\Database\Eloquent\Model;

class RequestConnection extends Model {

    protected $table = 'request_connections';
    protected $fillable = [
        'id',
        'name',
        'email',
        'mobile',
        'request_date',
        'address',
        'status',
        'is_archive',
        'created_by',
        'updated_by',
        'deleted_by',
        'created_at',
        'updated_at'
    ];

    public static function boot()
    {
        parent::boot();
        static::creating(function($data) {
            $data->created_by = auth()->user()->id;
            $data->updated_by = auth()->user()->id;
        });

        static::updating(function($data) {
            $data->updated_by = auth()->user()->id;
        });
    }

}
