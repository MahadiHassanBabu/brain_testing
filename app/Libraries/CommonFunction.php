<?php

namespace App\Libraries;

use Illuminate\Support\Carbon;

;

class CommonFunction {
    public static function dbDateFormat($date){
        if($date) {
            $date = str_replace('/','-',$date);
            return Carbon::parse($date)->toDateString();
        }
    }

    public static function appDateFormat($date){
        if($date) return Carbon::parse($date)->format('d/m/Y');

    }

    /*     * ****************************End of Class***************************** */
}
